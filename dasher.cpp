#include <cmath>
#include "raylib.h"
#include <iostream>
#include<array>
using namespace std;

struct AnimData
{
    Rectangle rect;
    Vector2 pos;
    int spriteRows;
    int spriteCols {1};
    int frame;
    int framesNumber;
    float updateTime;
    float runningTime;
};

void updateAnimData(AnimData &data,float DeltaTime) 
{
    /** UPDATING SPRITES
     * Get AnimData by reference, other useful variables too
     * Calculate the position of the sprite based on framerate
     * Increment index if less than framerate
     * Reset to zero once framerate reached
    */
    data.runningTime += DeltaTime;
    if(data.runningTime >= data.updateTime) 
    {
        data.runningTime = 0;
        data.frame++;
        data.rect.x = data.rect.width * data.frame;

        // Sprite sheets with multiple rows will make use of all of them
        if(data.spriteRows > 1)
            data.rect.y = data.rect.height * floor(data.frame / data.spriteRows);

        if(data.frame > data.framesNumber)
            data.frame = 0;
        
    }
}

void updateBackgroundStuff(float &posX, float DeltaTime, Texture2D background, int speed) 
{
    /** SETTING UP BACKGROUND 
     * If the background image 1 has passed the whole screen, reset its position X to 0
     * Move and update the position of the image 1, starting at 0
     * Move and update the position of the image 2, starting at the end of image 1
    */
    if(posX <= -background.width * 2)
        posX = 0;
    
    posX -= speed * DeltaTime;
    Vector2 bg1Pos {posX, 0.0};
    DrawTextureEx(background, bg1Pos, 0,  2.0, WHITE);

    Vector2 bg2Pos {(posX + background.width * 2), 0};
    DrawTextureEx(background, bg2Pos, 0,  2.0, WHITE);
}

void DrawCenteredText (const char *text, int posX, int posY, int fontSize, Color color) 
{
    /** THIS FUNCTION JUST CENTERS THE TEXT AND DRAWS IT */
    float positionX = posX - MeasureText(text, fontSize) / 2;
    DrawText(text, positionX, posY, fontSize, color);
}

int main() {
    /** SETUP WINDOW */
    const int windowDimensions [2] {512, 380};
    InitWindow(windowDimensions[0], windowDimensions[1], "Gamedev.tv dasher exercise");
    SetTargetFPS(60);

    /* CHARACTER SETUP */
    // Sprite
    Texture2D scarfyText {LoadTexture("./textures/scarfy.png")};
    AnimData scarfyData {
        {0, 0, scarfyText.width / 6, scarfyText.height}, // Rectangle
        {(windowDimensions[0] / 2 - scarfyData.rect.width / 2), 0}, // Starting pos
        1, 6, // Rows, cols
        0, // Starting frame
        5, // Number of frames
        1.0 / 10.0, // Frames per second (1 / FPS)
        0 // Time since last frame
    };

    // Physics stuff
    const int gravity {100}; // (px/s)/s
    Vector2 velocity {500, 0}; // Movement speed (left / right)
    bool touchingGround = false;
    const int jumpVelocity {-25}; // px/s

    /** NEBULAE SETUP */
    // Sprite
    Texture2D nebula = LoadTexture("textures/12_nebula_spritesheet.png");

    // Nebulae array setup
    const int nebulaeNumber {5};
    AnimData nebulaeData[nebulaeNumber] {};

    // AnimData setup for each nebula in array
    for(int i = 0; i < nebulaeNumber; i++) 
    {
        nebulaeData[i].rect = {0, 0, (float)nebula.width / 8, (float)nebula.height/8};
        nebulaeData[i].pos = {(float)windowDimensions[0] * (i + 1), windowDimensions[1] - nebulaeData[i].rect.height};
        nebulaeData[i].spriteCols = 8;
        nebulaeData[i].spriteRows = 8;
        nebulaeData[i].frame = 0;
        nebulaeData[i].framesNumber = 58;
        nebulaeData[i].updateTime = 1.0/25.0;
        nebulaeData[i].runningTime = 0;
    }

    // Movement
    int movingStuffVel {-100}; // px/s

    // Finish line
    float FinishLine {nebulaeData[nebulaeNumber - 1].pos.x + 100};

    /** BACKGROUND STUFF SETUP */
    // Textures
    Texture2D background {LoadTexture("textures/far-buildings.png")};
    Texture2D middleground {LoadTexture("textures/back-buildings.png")};
    Texture2D foreground {LoadTexture("textures/foreground.png")};

    // Variables
    float bgX{0.0};
    float mgX{0.0};
    float fgX{0.0};

    /** GAME VARIABLES */
    bool hasTouchedNebula {false};

    while(!WindowShouldClose()) 
    {
        BeginDrawing();
        ClearBackground(BLACK);

        /** GETTING DELTA TIME */
        const float DeltaTime = GetFrameTime();

        /** SETTING UP BACKGROUND */
        updateBackgroundStuff(bgX, DeltaTime, background, 20);
        updateBackgroundStuff(mgX, DeltaTime, middleground, 40);
        updateBackgroundStuff(fgX, DeltaTime, foreground, 80);

        /** HANDLE HORIZONTAL MOVEMENT
         * If the key A is pressed and not touching border, move to left
         * If the key D is pressed and not touching border, move to right
         * */
        if(IsKeyDown(KEY_A) && scarfyData.pos.x >= 0)
            scarfyData.pos.x -= velocity.x * DeltaTime;
        if(IsKeyDown(KEY_D) && scarfyData.pos.x <= (windowDimensions[0] - scarfyData.rect.width))
            scarfyData.pos.x += velocity.x * DeltaTime;

        /** UPDATING GRAVITY STUFF
         * First we check ifthe position isn't in the ground
         * If so, char isn't touching the ground, set that to false, update position and gravity
         * When false, we are touching ground, so speed to 0 and ensure position is ground
         * */
        if(scarfyData.pos.y <= windowDimensions[1] - scarfyData.rect.height) 
        {
            touchingGround = false; // Not touching ground
            velocity.y += gravity * DeltaTime; // Update gravity by changing speed
        }
        else
        {
            velocity.y = 0; // Reset velocity (down) to zero
            touchingGround = true; // Is touching ground
        }

        /** HANDLING JUMPING
         * First check if touching the ground (can't jump otherwise)
         * If so, check if the key is actually being pressed
         * If so, update velocity to initial jump speed, and set touchingGround to false
         * */
        if(touchingGround && IsKeyDown(KEY_SPACE)) 
        {
            velocity.y += jumpVelocity;
            touchingGround = false;
        }
        
        scarfyData.pos.y += velocity.y; // Move position up / down

        /** UPDATING PLAYER SPRITE */
        if(touchingGround)
            updateAnimData(scarfyData, DeltaTime);

        /** UPDATING EACH NEBULA */
        for(int i = 0; i < nebulaeNumber; i++) 
        {
            // Update nebula sprite
            updateAnimData(nebulaeData[i], DeltaTime);
            // Update nebula position
            nebulaeData[i].pos.x += movingStuffVel * DeltaTime;
        }

        /** CHECK COLLISIONS WITH NEBULA */
        for(AnimData nebula : nebulaeData) 
        {
            // Nebula collision bounds
            float padding{50}; // Reduces bounds of sprite
            Rectangle nebulaRec {
                nebula.pos.x + padding,
                nebula.pos.y + padding,
                nebula.rect.width - 2 * padding,
                nebula.rect.height - 2 * padding
            };
            
            // Scarfy collision bounds
            Rectangle scarfyRec {
                scarfyData.pos.x,
                scarfyData.pos.y,
                scarfyData.rect.width,
                scarfyData.rect.height
            };
            
            // Collision found
            if(!hasTouchedNebula)
                hasTouchedNebula = CheckCollisionRecs(nebulaRec, scarfyRec);
        }
        
        FinishLine += movingStuffVel * DeltaTime;

        /** CHECK IF GAME HAS BEEN LOST
         * Just do so if it hasn't collidied with nebula (lost)
         */
        if(hasTouchedNebula) 
        {
            // Game lost
            DrawCenteredText("YOU LOST :(", windowDimensions[0] / 2, windowDimensions[1] / 2, 50, RED);
        } 
        else if(scarfyData.pos.x > FinishLine) 
        {
            // Game won
            DrawCenteredText("YOU WON MOTHARFUCKA", windowDimensions[0] / 2, windowDimensions[1] / 2, 30, PURPLE);
        }
        else 
        {
            // Game not lost, continue
            DrawTextureRec(scarfyText, scarfyData.rect, scarfyData.pos, WHITE);
            for(AnimData nebulas : nebulaeData) {
                DrawTextureRec(nebula, nebulas.rect, nebulas.pos, WHITE);
            }
        }

        EndDrawing();
    }

    // Unload textures before closing
    UnloadTexture(scarfyText);
    UnloadTexture(nebula);
    UnloadTexture(background);
    UnloadTexture(middleground);
    UnloadTexture(foreground);

    CloseWindow();
}